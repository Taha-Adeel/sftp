package org.sftp.example;

import org.sftp.core.SftpServer;

// Set up a SFTP server
public class SftpServerApp {
    public static void main(String[] args) {
        // Create a SshServer
        SftpServer sshd = new SftpServer("server-user", "server-password", 2225, "./src/test/resources/server_key.pem");
        sshd.setRootDir("server_test");

        // Start the server
        try {
            sshd.start();

            // Wait for user to press any key to stop the server
            System.out.println("Press any key to stop the server");
            //noinspection ResultOfMethodCallIgnored
            System.in.read();

            // Stop the server
            sshd.stop();
        } catch (Exception e) {
            System.out.println("Failed to start SFTP server");
            e.printStackTrace();
        }
    }
}