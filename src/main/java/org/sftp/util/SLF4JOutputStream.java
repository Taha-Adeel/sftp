package org.sftp.util;

import java.io.IOException;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to redirect std out to SLF4J logger so that the outputs are in sync with the log messages.
 */
public class SLF4JOutputStream extends OutputStream {
    private final Logger logger;
    private final StringBuilder buffer;

    public SLF4JOutputStream() {
        logger = LoggerFactory.getLogger("STDOUT");
        buffer = new StringBuilder();
    }

    @Override
    public void write(int b) throws IOException {
        if (b == '\n') {
            flushBuffer();
        } else {
            buffer.append((char) b);
        }
    }

    private void flushBuffer() {
        if (buffer.length() > 0) {
            logger.info(buffer.toString().trim());
            buffer.setLength(0);
        }
    }

    @Override
    public void flush() throws IOException {
        flushBuffer();
    }

    @Override
    public void close() throws IOException {
        flushBuffer();
    }
}


