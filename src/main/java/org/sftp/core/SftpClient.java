package org.sftp.core;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.config.hosts.HostConfigEntry;
import org.apache.sshd.client.session.ClientSession;
import org.apache.sshd.common.io.nio2.Nio2ServiceFactoryFactory;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.apache.sshd.sftp.client.SftpClientFactory;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.LoggerFactory;
import com.google.common.base.Stopwatch;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.*;
import java.io.FileReader;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Objects;

import static org.apache.sshd.common.config.keys.KeyUtils.generateKeyPair;

/**
 * <p>Connect to a sftp server and perform sftp operations such as get/put files into the sftp server </p>
 *
 * <p> Wrapper for the apache mina sshd sftp client, which can be accessed using getSftpClient(), through which lower level API calls can be made. The MySftpClient class is thread-safe. </p>
 *
 * <p> Internally uses a static SshClient object to reuse the same ssh client for multiple sftp connections,
 * possibly to different sftp servers </p>
 *
 */
public class SftpClient extends AbstractLoggingBean {
    /** The static ssh client used to connect to the sftp servers and obtain {@link ClientSession}s */
    private static SshClient sshClient = null;
    {
        if(sshClient == null) {
            sshClient = SshClient.setUpDefaultClient();
            sshClient.setIoServiceFactoryFactory(new Nio2ServiceFactoryFactory());
            sshClient.start(); // Start the ssh client.
            log.info("Ssh client instance created and started.\n");
        }
    }
    public static SshClient getSshClient() {
        return sshClient;
    }

    /** The static SftpClientFactory used to construct {@link SftpClient}s from {@link ClientSession}s */
    private static final SftpClientFactory factory = SftpClientFactory.instance();

    /** The client session obtained by {@link SshClient} on connecting to the ssh server using the server's hostname, port, username and password/key. */
    private ClientSession session = null;
    public ClientSession getSession() {
        return session;
    }

    /** The sftpClient interface provided by apache mina that has API calls to perform file/directory operations on the sftp server */
    public org.apache.sshd.sftp.client.SftpClient sftpClient = null;
    public org.apache.sshd.sftp.client.SftpClient getSftpClient() {
        return sftpClient;
    }

    // Constructors
    public SftpClient() {super(LoggerFactory.getLogger(SftpClient.class.getSimpleName()));}
    public SftpClient(String configFilePath) throws IOException { this();
        connect(configFilePath);
    }
    public SftpClient(String host, int port, String username, String password) throws IOException { this();
        connect(host, port, username, password);
    }
    public SftpClient(String host, int port, String username, KeyPair keyPair) throws IOException { this();
        connect(host, port, username, keyPair);
    }

    /**
     * Connect to the sftp server using the specified json config file, and authenticate using password authentication.
     *
     * <p> Closes any existing connection to a sftp server before connecting to the new sftp server. </p>
     *
     * @param configFilePath The path to the json config file
     * @throws IOException If failed to resolve the effective remote address or connect to it, or if the client verification timed out, or if the sftp client creation failed.
     */
    public void connect(String configFilePath) throws IOException {
        log.info("Connecting to sftp server using config file: " + configFilePath);
        if (sftpClient != null) {
            log.info("Disconnecting from existing sftp server.");
            disconnect(); // Close any existing connection to a sftp server.
        }

        try (FileReader reader = new FileReader(configFilePath)) {
            JSONObject jsonObject = new JSONObject(new JSONTokener(reader));

            String host = jsonObject.getString("host");
            int port = jsonObject.getInt("port");
            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");
            connect(host, port, username, password);
        } catch (IOException e) {
            log.error("Failed to connect to sftp server using config file: " + configFilePath + "\n");
            e.printStackTrace();
        }
    }

    /**
     * Connect to the sftp server using the specified hostname, port, username and password, and authenticate using password authentication.
     *
     * <p> Closes any existing connection to a sftp server before connecting to the new sftp server. </p>
     *
     * @param host The hostname of the sftp server - never null/empty
     * @param port The port of the sftp server
     * @param username The username to use to connect to the sftp server
     * @param password The password to use to connect to the sftp server - may not be null/empty
     * @throws IOException If failed to resolve the effective remote address or connect to it, or if the client verification timed out, or if the sftp client creation failed.
     */
    public void connect(String host, int port, String username, String password) throws IOException {
        log.info("Connecting to sftp server using host: " + host + ", port: " + port + ", username: " + username + ", password: " + password + "\n");
        if (sftpClient != null) {
            log.info("Disconnecting from existing sftp server.");
            disconnect(); // Close any existing connection to a sftp server.
        }

        session = sshClient.connect(username, host, port).verify().getSession();
        session.addPasswordIdentity(password);
        session.auth().verify();
        log.info("Client session created and authenticated using password authentication.");

        sftpClient = factory.createSftpClient(session);
        log.info("Connected to sftp server using host: " + host + ", port: " + port + ", username: " + username + ", password: " + password + "\n");
    }

    /**
     * Connect to the sftp server using the specified hostname, port, username and key, and authenticate using public key authentication.
     * Assumes that the valid keys are stored in the server's authorized_keys file.
     *
     * <p> Closes any existing connection to a sftp server before connecting to the new sftp server. </p>
     *
     * @param host The hostname of the sftp server - never null/empty
     * @param port The port of the sftp server
     * @param username The username to use to connect to the sftp server
     * @param key The key to use to connect to the sftp server - may not be null/empty
     * @throws IOException If failed to resolve the effective remote address or connect to it, or if the client verification timed out, or if the sftp client creation failed.
     */
    public void connect(String host, int port, String username, KeyPair key) throws IOException {
        log.info("Connecting to sftp server using host: " + host + ", port: " + port + ", username: " + username + ", key: " + key + "\n");
        if (sftpClient != null) {
            log.info("Disconnecting from existing sftp server.");
            disconnect(); // Close any existing connection to a sftp server.
        }

        session = sshClient.connect(username, host, port).verify().getSession();
        session.addPublicKeyIdentity(key);
        session.auth().verify();
        log.info("Client session created and authenticated using public key authentication.");

        sftpClient = factory.createSftpClient(session);
        log.info("Connected to sftp server using host: " + host + ", port: " + port + ", username: " + username + ", key: " + key + "\n");
    }

    /**
     * Connect to the sftp server through a jump proxy using the specified json config file
     * @param configFilePath The path to the json config file
     * @throws IOException If failed to resolve the effective remote address or connect to it, or if the client verification timed out, or if the sftp client creation failed.
     */
    public void connectJump(String configFilePath) throws IOException {
        log.info("Connecting to sftp server through jump proxy using config file: " + configFilePath);
        if (sftpClient != null) {
            log.info("Disconnecting from existing sftp server.");
            disconnect(); // Close any existing connection to a sftp server.
        }

        FileReader reader = new FileReader(configFilePath);
        JSONObject jsonObject = new JSONObject(new JSONTokener(reader));

        String host = jsonObject.getString("host");
        int port = jsonObject.getInt("port");
        String username = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        String jumpHost = jsonObject.getString("jump-host");
        String jumpUsername = jsonObject.getString("jump-username");
        int jumpPort = jsonObject.getInt("jump-port");
        connectJump(host, port, username, password, jumpHost, jumpUsername, jumpPort);
    }

    /**
     * Connect to the sftp server through a jump proxy using the specified hostname, port, username and password, and authenticate using password authentication.
     * @param host The hostname of the sftp server - never null/empty
     * @param port The port of the sftp server
     * @param username The username to use to connect to the sftp server
     * @param password The password to use to connect to the sftp server - may not be null/empty
     * @param jumpHost The hostname of the jump proxy server - never null/empty
     * @param jumpUsername The username to use to connect to the jump proxy server
     * @param jumpPort The port of the jump proxy server
     * @throws IOException If failed to resolve the effective remote address or connect to it, or if the client verification timed out, or if the sftp client creation failed.
     */
    public void connectJump(String host, int port, String username, String password, String jumpHost, String jumpUsername, int jumpPort) throws IOException {
        log.info("Connecting to sftp server through jump proxy using host: " + host + ", port: " + port + ", username: " + username + ", password: " + password + ", jumpHost: " + jumpHost + ", jumpUsername: " + jumpUsername + ", jumpPort: " + jumpPort + "\n");
        if (sftpClient != null) {
            log.info("Disconnecting from existing sftp server.");
            disconnect(); // Close any existing connection to a sftp server.
        }

        String proxyJump = jumpUsername+"@"+jumpHost+":"+jumpPort;
        session = sshClient.connect(new HostConfigEntry("", host, port, username, proxyJump)).verify().getSession();
        session.addPasswordIdentity(password);
        session.auth().verify();
        log.info("Client session created and authenticated using jump proxy in between.");

        sftpClient = factory.createSftpClient(session);
        log.info("Connected to sftp server through jump proxy using host: " + host + ", port: " + port + ", username: " + username + ", password: " + password + ", jumpHost: " + jumpHost + ", jumpUsername: " + jumpUsername + ", jumpPort: " + jumpPort + "\n");
    }

    /**
     * Disconnect from the sftp server.
     *
     * @throws IOException If failed to close the sftp client or the client session.
     */
    public void disconnect() throws IOException {
        if(sftpClient != null) sftpClient.close();
        if(session != null) session.close();
        sftpClient = null;
        session = null;
        log.info("Disconnected from sftp server.\n");
    }

    //
    // File operations
    //

    /**
     * Display remote directory listing of the current remote directory.
     */
    public void ls() throws IOException {
        ls(".", null);
    }

    /**
     * Display remote directory listing for the specified remote directory.
     *
     * @param remotePath The remote directory to list - may not be null/empty
     */
    public void ls(String remotePath) throws IOException {
        ls(remotePath, null);
    }

    /**
     * Display remote directory listing for the specified remote directory.
     *
     * @param remotePath The remote directory to list - may not be null/empty
     * @param options The options to use for the ls command - may be null. a -> display hidden files, l-> display long listing
     * @throws IOException If failed to read the remote directory entries.
     * @throws IllegalStateException If not connected to a sftp server.
     */
    public void ls(String remotePath, String options) throws IOException {
        log.info("Listing remote directory: \"" + remotePath + "\", options: \"" + options + "\"");
        if (sftpClient == null) {
            log.error("Not connected to a sftp server before attempting to list remote directory.");
            throw new IllegalStateException("Not connected to a sftp server.");
        }

        java.util.Collection<org.apache.sshd.sftp.client.SftpClient.DirEntry> dirEntries = sftpClient.readEntries(remotePath);
        for (org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry : dirEntries) {
            // Skip hidden files if the options do not contain "a"
            if((options == null || !options.contains("a")) && dirEntry.getFilename().startsWith(".")) continue;

            // Display long listing if the options contain "l"
            if(options != null && options.contains("l")) {
                System.out.print(" " + dirEntry.getAttributes().getPermissions());
                System.out.print(" " + dirEntry.getAttributes().getOwner());
                System.out.print(" " + dirEntry.getAttributes().getGroup());
                System.out.print(" " + dirEntry.getAttributes().getSize());
                System.out.print(" " + dirEntry.getAttributes().getAccessTime());
                System.out.print(" " + dirEntry.getAttributes().getModifyTime());
            }

            // Display the filename
            System.out.println("\t" + dirEntry.getFilename());
        }
        System.out.println();
    }

    /**
     * Get (Download) the specified remote file to a local file with the same name in the current local directory.
     * @param remotePath The remote file to get - may not be null/empty
     * @throws IOException If failed to read the remote file or write the local file.
     */
    public void get(String remotePath) throws IOException {
        get(remotePath, null);
    }

    /**
     * Get (Download) the specified remote file to a local file with the specified name in the current local directory.
     * @param remotePath The remote file to get - may not be null/empty
     * @param localName The local file name for the downloaded file
     * @throws IOException If failed to read the remote file or write the local file, or if local file path does not exist
     */
    public void get(String remotePath, String localName) throws IOException {
        Stopwatch timer = Stopwatch.createStarted();
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to get remote file: \"" + remotePath + "\"\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }
        if (localName == null || localName.isEmpty()) {
            localName = "./"+Paths.get(remotePath).getFileName().toString();
        }
        // If the path is neither relative nor absolute, then consider it relative to the current directory
        if(!Paths.get(localName).isAbsolute() && !Paths.get(localName).startsWith(".")) {
            localName = "./"+localName;
        }
        log.info("Getting remote file: \"" + remotePath + "\" to local file: \"" + localName + "\"");

        // Recursively create local directory if it does not exist
        File localFilePath = new File(localName);
        if(localFilePath.getParentFile().mkdirs()) {
            log.info("Created local missing parent directory: " + localFilePath.getParentFile().getAbsolutePath());
        }
        else if(!localFilePath.getParentFile().exists()) {
            log.error("Failed to create local directory: " + localFilePath.getParentFile().getAbsolutePath());
            throw new IOException("Failed to create local directory: " + localFilePath.getParentFile().getAbsolutePath());
        }

        // Open the remote file for reading
        log.info("Opening remote file: \"" + remotePath + "\" for reading");
        try (FileChannel remoteFile = sftpClient.openRemoteFileChannel(remotePath)) {
            // Create the local file and copy the contents from the remote file
            log.info("Creating local file: \"" + localName + "\" and copying contents from remote file.");
            try (FileChannel localFile = FileChannel.open(Paths.get(localName), StandardOpenOption.CREATE, StandardOpenOption.WRITE)) {
                localFile.transferFrom(remoteFile, 0, remoteFile.size());
            }
            catch (IOException e) {
                log.error("Failed to create local file: \"" + localName + "\" and copy contents from remote file.\n");
                throw e;
            }
            log.info("Downloaded remote file \"" + remotePath + "\" [" + remoteFile.size() + " bytes] in " + timer.stop() + "\n");
        }
        catch (IOException e) {
            log.error("Failed to get remote file: \"" + remotePath + "\" to local file: \"" + localName + "\"\n");
            throw e;
        }
    }

    /**
     * Get (Download) the specified remote directory to a local directory recursively, with the same name as the remote directory.
     * @param remotePath The remote directory to get - may not be null/empty
     * @throws IOException If failed to read the remote directory or write the local directory.
     */
    public void getDir(String remotePath) throws IOException {
        getDir(remotePath, null);
    }

    /**
     * Get (Download) the specified remote directory to a local directory recursively.
     * @param remotePath The remote directory to get - may not be null/empty
     * @param localPath The local directory to download the remote directory to. If null/empty, the remote directory name will be used.
     * @throws IOException If failed to read the remote directory or write the local directory, or if local directory path does not exist
     */
    public void getDir(String remotePath, String localPath) throws IOException {
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to get remote directory: \"" + remotePath + "\"\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }
        if (localPath == null || localPath.isEmpty()) {
            localPath = "./"+Paths.get(remotePath).getFileName().toString();
        }

        log.info("Getting remote directory: \"" + remotePath + "\" to local directory: \"" + localPath + "\"");


        // Recursively create local directory if it does not exist
        File localFilePath = new File(localPath);
        if(localFilePath.mkdirs()) {
            log.info("Created local missing parent directory: " + localFilePath.getAbsolutePath());
        }
        else if(!localFilePath.exists()) {
            log.error("Failed to create local directory: " + localFilePath.getAbsolutePath() + "\n");
            throw new IOException("Failed to create local directory: " + localFilePath.getAbsolutePath());
        }

        // Recursively get all files in the remote directory
        Collection<org.apache.sshd.sftp.client.SftpClient.DirEntry> dirEntries = sftpClient.readEntries(remotePath);
        for (org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry : dirEntries) {
            // Skip hidden files
            if(dirEntry.getFilename().startsWith(".")) continue;

            // Get the remote file path
            String remoteFilePath = remotePath + "/" + dirEntry.getFilename();

            // Get the local file path
            String localFilePath2 = localPath + "/" + dirEntry.getFilename();

            // If the remote file is a directory, recursively get all files in the remote directory
            if(dirEntry.getAttributes().isDirectory()) {
                getDir(remoteFilePath, localFilePath2);
            }
            else {
                // Get the remote file
                get(remoteFilePath, localFilePath2);
            }
        }
    }

    /**
     * Put (Upload) the specified local file to the current remote directory using the same file name as the local file.
     * @param localPath The local file to put - may not be null/empty
     * @throws IOException If failed to read the local file or write the remote file.
     */
    public void put(String localPath) throws IOException {
        put(localPath, null);
    }

    /**
     * Put (Upload) the specified local file to the current remote directory using the specified remote file name.
     * @param localPath The local file to put - may not be null/empty
     * @param remotePath The remote file name for the uploaded file
     * @throws IOException If failed to read the local file or write the remote file, or if remote file path does not exist
     */
    public void put(String localPath, String remotePath) throws IOException {
        Stopwatch timer = Stopwatch.createStarted();
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to put local file: \"" + localPath + "\"\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }

        if (remotePath == null || remotePath.isEmpty()) {
            remotePath = "./" + Paths.get(localPath).getFileName().toString();
        }

        // If the remote path is neither relative nor absolute, consider it relative to the current remote directory
        if (!remotePath.startsWith("/") && !remotePath.startsWith(".")) {
            remotePath = "./" + remotePath;
        }
        log.info("Putting local file: \"" + localPath + "\" to remote file: \"" + remotePath + "\"");

        // Open the local file for reading
        log.info("Opening local file: \"" + localPath + "\" for reading");
        FileChannel localFile = FileChannel.open(Paths.get(localPath), StandardOpenOption.READ);

        // Iterate over the [n-1] path folders, creating them if they don't exist
        String[] folders = remotePath.split("/");
        String path = ".";
        for (int i = 0; i < folders.length - 1; i++) {
            if (folders[i].isEmpty() || folders[i].equals(".")) continue;
            path += "/" + folders[i];
            try {
                sftpClient.stat(path);
            }
            catch (IOException e) {
                log.info("Creating remote directory: \"" + path + "\"");
                sftpClient.mkdir(path);
            }
        }

        // Create the remote file and copy the contents from the local file
        log.info("Creating remote file: \"" + remotePath + "\" and copying contents from local file.");
        try (FileChannel remoteFile = sftpClient.openRemoteFileChannel(remotePath, EnumSet.of(org.apache.sshd.sftp.client.SftpClient.OpenMode.Write, org.apache.sshd.sftp.client.SftpClient.OpenMode.Create))) {
            remoteFile.transferFrom(localFile, 0, localFile.size());
            log.info("Uploaded local file: \"" + localPath + "\" [" + localFile.size() + " bytes] in " + timer.stop() + "\n");
        }
        catch (IOException e) {
            log.error("Failed to create remote file: \"" + remotePath + "\" and copy contents from local file.\n");
            throw e;
        }

        // Close the local file
        localFile.close();

    }

    /**
     * Put (Upload) the specified local directory to the current remote directory recursively, with the same name as the local directory.
     * @param localPath The local directory to put - may not be null/empty
     * @throws IOException If failed to read the local directory or write the remote directory.
     */
    public void putDir(String localPath) throws IOException {
        putDir(localPath, null);
    }

    /**
     * Put (Upload) the specified local directory to the current remote directory recursively.
     * @param localPath The local directory to put - may not be null/empty
     * @param remotePath The remote directory to upload the local directory to. If null/empty, the local directory name will be used.
     * @throws IOException If failed to read the local directory or write the remote directory, or if remote directory path does not exist
     */
    public void putDir(String localPath, String remotePath) throws IOException {
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to put local directory: \"" + localPath + "\"\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }
        if (remotePath == null || remotePath.isEmpty()) {
            remotePath = "./"+Paths.get(localPath).getFileName().toString();
        }
        log.info("Putting local directory: \"" + localPath + "\" to remote directory: \"" + remotePath + "\"");

        // Iterate over the path folders, creating them if they don't exist
        String[] folders = remotePath.split("/");
        String path = ".";
        for (String folder : folders) {
            if (folder.isEmpty()) continue;
            path += "/" + folder;
            try {
                sftpClient.stat(path);
            } catch (IOException e) {
                // Create the remote directory
                log.info("Creating remote directory: \"" + path + "\"");
                sftpClient.mkdir(path);
            }
        }

        // Recursively put all files in the local directory
        File localFilePath = new File(localPath);
        for (File localFile : Objects.requireNonNull(localFilePath.listFiles())) {
            // Skip hidden files
            if(localFile.getName().startsWith(".")) continue;

            // Get the local file path
            String localFilePath2 = localPath + "/" + localFile.getName();

            // Get the remote file path
            String remoteFilePath = remotePath + "/" + localFile.getName();

            // If the local file is a directory, recursively put all files in the local directory
            if(localFile.isDirectory()) {
                putDir(localFilePath2, remoteFilePath);
            }
            else {
                // Put the local file
                put(localFilePath2, remoteFilePath);
            }
        }
    }

    /**
     * Delete the specified remote file.
     * @param remotePath The remote file to delete - may not be null/empty
     * @throws IOException If failed to delete the remote file.
     */
    public void rm(String remotePath) throws IOException {
        log.info("Deleting remote file: \"" + remotePath + "\"");
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to delete remote file: \"" + remotePath + "\"\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }

        sftpClient.remove(remotePath);
        log.info("Deleted remote file: \"" + remotePath + "\"\n");
    }

    /**
     * Delete the specified remote directory, and all its contents recursively.
     * @param remotePath The remote directory to delete - may not be null/empty
     * @throws IOException If failed to delete the remote directory.
     */
    public void rmdir(String remotePath) throws IOException {
        log.info("Deleting remote directory: \"" + remotePath + "\" along with all its contents recursively");
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to delete remote directory\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }

        for (org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry : sftpClient.readEntries(remotePath)) {
            if (dirEntry.getFilename().equals(".") || dirEntry.getFilename().equals("..")) {
                continue;
            }
            if (dirEntry.getAttributes().isDirectory()) {
                rmdir(remotePath + "/" + dirEntry.getFilename());
            } else {
                rm(remotePath + "/" + dirEntry.getFilename());
            }
        }

        sftpClient.rmdir(remotePath);
        log.info("Deleted remote directory: \"" + remotePath + "\" along with all its contents recursively\n");
    }


    //
    // Key Pair Management
    //

    /**
     * Load the private and public keys from the provided filename and filename.pub files respectively.
     * @return The KeyPair containing the private and public keys
     * @throws IOException If failed to read the private or public key files
     * @throws NoSuchAlgorithmException If the RSA algorithm is not supported
     */
    public KeyPair loadKeyPair(String keyFilename) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException, GeneralSecurityException {
        String publicKeyFilePath = keyFilename + ".pub";
        log.info("Loading private key from: \"" + keyFilename + "\" and public key from: \"" + publicKeyFilePath + "\"");

        File privateKeyFile = new File(keyFilename);
        File publicKeyFile = new File(publicKeyFilePath);

        try (FileInputStream privateKeyInputStream = new FileInputStream(privateKeyFile);
             FileInputStream publicKeyInputStream = new FileInputStream(publicKeyFile)) {

            byte[] privateKeyBytes = new byte[(int) privateKeyFile.length()];
            privateKeyInputStream.read(privateKeyBytes);

            byte[] publicKeyBytes = new byte[(int) publicKeyFile.length()];
            publicKeyInputStream.read(publicKeyBytes);

            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            KeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);

            KeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            log.info("Loaded private key from: \"" + keyFilename + "\" and public key from: \"" + publicKeyFilePath + "\"\n");
            return new KeyPair(publicKey, privateKey);
        }
        catch (IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
            log.info("Generating new key pair.\n");
            // Generate and return a new key pair
            return KeyPairGenerator.getInstance("RSA").generateKeyPair();
        }
    }

    /**
     * Add the specified public key to the authorized_keys file for the current user.
     * @param key The public key to add to the authorized_keys file
     * @throws IOException If failed to read the authorized_keys file or write to it
     */
    public void addToAuthorizedKeys(String key) throws IOException {
        log.info("Adding public key to authorized_keys file");
        if (sftpClient == null) {
            log.error("Not connected to a sftp server while trying to add public key to authorized_keys file\n");
            throw new IllegalStateException("Not connected to a sftp server");
        }
        // Open the authorized_keys file for appending
        log.info("Opening authorized_keys file for appending");
        try (FileChannel authorizedKeysFile = sftpClient.openRemoteFileChannel("~/.ssh/authorized_keys", EnumSet.of(org.apache.sshd.sftp.client.SftpClient.OpenMode.Write, org.apache.sshd.sftp.client.SftpClient.OpenMode.Append))) {
            // Append the public key to the authorized_keys file
            authorizedKeysFile.write(ByteBuffer.wrap(key.getBytes()));
        }
        catch (IOException e) {
            log.error("Failed to add public key to authorized_keys file\n");
            throw e;
        }
        log.info("Added public key to authorized_keys file\n");
    }
}
