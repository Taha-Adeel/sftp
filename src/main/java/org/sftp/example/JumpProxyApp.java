package org.sftp.example;

import org.sftp.core.JumpProxy;

public class JumpProxyApp {
    public static void main(String[] args) {
        JumpProxy proxy = new JumpProxy("proxy-user", "proxy-password", 2222, "src/test/resources/proxy_server_key.pem", null);

        try {
            // Start the proxy server
            proxy.start();

            System.out.println("Input any key to stop the proxy server");
            var temp = System.in.read();

            proxy.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
