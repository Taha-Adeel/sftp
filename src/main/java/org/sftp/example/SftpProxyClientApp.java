package org.sftp.example;

import org.sftp.core.SftpClient;
import org.sftp.core.SftpServer;
import org.sftp.util.ChannelEventLogger;
import org.sftp.util.PortForwardingEventLogger;
import org.sftp.util.SessionEventLogger;

public class SftpProxyClientApp {
    public static void main(String[] args) {
        // Create a sftp client and add event listeners
        SftpClient client = new SftpClient();
        SftpClient.getSshClient().addPortForwardingEventListener(new PortForwardingEventLogger("Client"));
        SftpClient.getSshClient().addChannelListener(new ChannelEventLogger("Client"));
        SftpClient.getSshClient().addSessionListener(new SessionEventLogger("Client"));

        try {
            // Connect to the sftp server via the proxy server using the below config file
            String configFilePath = "./src/test/resources/local-sftp-config.json";
            client.connectJump(configFilePath);

            // Perform SFTP operations

            // List files in the current remote directory
            client.ls();

            // List files in ./src dir with flags -l
            client.ls(".", "-la");

            // List files in current dir with flags -la
            client.ls("./dir", "-a");

            // Get file ReadMe.md and save it in ./Downloads/ReadMe.md
            client.get("./A.txt", "./Downloads/A_copy.txt");

            // Put the file ./Downloads/A_copy.txt into ./Uploads/A_copy.txt on the remote server
            client.put("./Downloads/A_copy.txt", "./Uploads/A_copy.txt");

            // Delete the file ./Uploads/A_copy.txt
            client.rm("./Uploads/A_copy.txt");

            // Get the remote root directory and save it in ./server_files
            client.getDir(".", "./server_files");

            // Put the directory ./server_files/dir into ./Uploads/dir on the remote server
            client.putDir("./server_files/dir", "./Uploads/dir");

            // Delete the directory ./Uploads/dir
            client.rmdir("./Uploads");

            client.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

