# SFTP Server and Client with Proxy Support

This project contains the implementation of an SFTP Server and Client with Proxy Support using the Apache Mina SSHD library. Sample usages can be seen in the org.sftp.example module and class documentation can be found in the documentation folder (./documentation/index.html). 

## Source Code Structure

### org.sftp.core module: 
Contains the core code that contains the logic for setting up the different classes.
* SftpClient.java: Contains the SFTP Client implementation
* SftpServer.java: Contains the SFTP Server implementation
* JumpProxy.java: Contains the Jump Proxy implementation

### org.sftp.example module:
Contains example driver codes using the SFTP Client, Server and proxy from the org.sftp.core module.
* SftpServerApp: This application starts an SFTP server on the specified port and root directory (2225 and "server_test" in this example).
* SftpClientApp: This application starts an SFTP client that connects to the SFTP server started using SftpServerApp, and performs sample sftp operations.
* JumpProxyApp: This application starts a jump proxy on the specified port (2222 in this example), which can be used by the SFTP client to connect to the SFTP server.
* SftpProxyClientApp: This application starts an SFTP client that connects to the SFTP server started using SftpServerApp, using the jump proxy started using JumpProxyApp, and performs sample sftp operations.
* SprinklrClientApp: This application starts an SFTP client that connects to the sprinklr sftp server using the configuration details in src/test/resources/sprinklr-sftp-config.json.

### org.sftp.util module:
Contains utility classes that help log the SFTP operations and connection details using slf4j logger and event listeners.

### org.sftp.test module:
Contains unit tests for the SFTP Client and SFTP Server implementations in the org.sftp.core module.
* SftpClientTest: Contains unit tests for the SFTP Client implementation.
* SftpServerTest: Contains unit tests for the SFTP Server implementation.

## Documentation
Class documentation available at [./documentation/index.html](./documentation/index.html) (Open in browser)

## Report
Report link: [Report](https://sprinklr-my.sharepoint.com/personal/nabagata_saha_sprinklr_com/_layouts/15/onedrive.aspx?ct=1689037603272&or=Teams%2DHL&ga=1&LOF=1&id=%2Fpersonal%2Fnabagata%5Fsaha%5Fsprinklr%5Fcom%2FDocuments%2FIntegration%20Interns%2FTaha%20Adeel%20Mohammed%2FProject%20Report%2Epdf&parent=%2Fpersonal%2Fnabagata%5Fsaha%5Fsprinklr%5Fcom%2FDocuments%2FIntegration%20Interns%2FTaha%20Adeel%20Mohammed)
