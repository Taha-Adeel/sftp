package org.sftp.core;

import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.common.io.nio2.Nio2ServiceFactoryFactory;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.pubkey.AcceptAllPublickeyAuthenticator;
import org.apache.sshd.server.config.keys.AuthorizedKeysAuthenticator;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.sftp.server.SftpSubsystemFactory;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Collections;

/**
 * {@link SftpServer} sets up an SSH server with an SFTP subsystem and a virtual/native filesystem on which SFTP operations can be performed. The server can be connected to using password authentication or with ssh key pairs.
 *
 * <p>Creates a new nio2 thread for each client that connects to the SFTP server and is thread-safe.</p>
 *
 *
 * Example usage:
 * <pre>
 *     {@code
 *     // Create a new SFTP server
 *     SftpServer sftpServer = new SftpServer(username, password, port, keyFilePath);
 *     // Start the server
 *     sftpServer.start();
 *     // Stop the server
 *     sftpServer.stop();
 *     }
 * </pre>
 *
 */
public class SftpServer extends AbstractLoggingBean {
    private final SshServer sshServer;

    /**
     * Constructs a new instance of {@link SftpServer} with the specified parameters.
     *
     * @param username    the username for authentication
     * @param password    the password for authentication
     * @param port        the port on which the server will listen
     * @param keyFilePath the file path to the SSH host key
     */
    public SftpServer(String username, String password, int port, String keyFilePath) {
        super(LoggerFactory.getLogger(SftpServer.class.getSimpleName()));
        sshServer = SshServer.setUpDefaultServer();

        sshServer.setPort(port);
        sshServer.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(Paths.get(new File(keyFilePath).getAbsolutePath())));
        sshServer.setPasswordAuthenticator((user, pass, session) -> user.equals(username) && pass.equals(password));
        sshServer.setSubsystemFactories(Collections.singletonList(new SftpSubsystemFactory.Builder().build()));
        sshServer.setIoServiceFactoryFactory(new Nio2ServiceFactoryFactory());
//        setAuthorizedKeysFilePath(null);

        log.info("Sftp server set up on port " + port + " with username: \"" + username + "\" and password: \"" + password + "\"");
    }

    /**
     * Returns the underlying SSH server instance.
     *
     * @return the SSH server instance
     */
    public SshServer getSshServer() {
        return sshServer;
    }

    /**
     * Starts the SFTP server.
     *
     * @throws IOException if an I/O error occurs while starting the server
     */
    public void start() throws IOException {
        sshServer.start();
        log.info("Sftp server started. \n");
    }

    /**
     * Stops the SFTP server.
     *
     * @throws IOException if an I/O error occurs while stopping the server
     */
    public void stop() throws IOException {
        sshServer.stop();
        log.info("Sftp server stopped. \n");
    }

    /**
     * Sets the root directory for the SFTP server.
     * @param directory the root directory
     */
    public void setRootDir(String directory) {
        VirtualFileSystemFactory fileSystemFactory = new VirtualFileSystemFactory();
        fileSystemFactory.setDefaultHomeDir(Paths.get(new File(directory).getAbsolutePath()));
        sshServer.setFileSystemFactory(fileSystemFactory);
    }


    /**
     * Sets the authorized keys file path.
     * @param authorizedKeysFilePath the file path to the authorized keys
     */
    public void setAuthorizedKeysFilePath(String authorizedKeysFilePath) {
        if(authorizedKeysFilePath == null) {
            sshServer.setPublickeyAuthenticator(AcceptAllPublickeyAuthenticator.INSTANCE);
            log.info("No authorized keys file path set for the sftp server. All public keys will be accepted");
            return;
        }
        sshServer.setPublickeyAuthenticator(new AuthorizedKeysAuthenticator(Paths.get(new File(authorizedKeysFilePath).getAbsolutePath())));
        log.info("Authorized keys file path for the sftp server set to " + authorizedKeysFilePath + "\n");
    }
}