package org.sftp.util;

import org.apache.sshd.common.forward.PortForwardingEventListener;
import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.apache.sshd.common.util.net.SshdSocketAddress;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * A {@link PortForwardingEventListener} that logs the port forwarding events.
 */
public class PortForwardingEventLogger extends AbstractLoggingBean implements PortForwardingEventListener {
    String name; // Name of the class using this logger
    public PortForwardingEventLogger(String name) {
        super(LoggerFactory.getLogger(PortForwardingEventLogger.class.getSimpleName()));
        this.name = name;
    }

    /**
     * Signals the attempt to establish a local/remote port forwarding
     *
     * @param  session         The {@link Session} through which the attempt is made
     * @param  local           The local address - may be {@code null} on the receiver side
     * @param  remote          The remote address - may be {@code null} on the receiver side
     * @param  localForwarding Local/remote port forwarding indicator
     * @throws IOException     If failed to handle the event - in which case the attempt is aborted and the exception
     *                         re-thrown to the caller
     */
    public void establishingExplicitTunnel(
            Session session, SshdSocketAddress local, SshdSocketAddress remote, boolean localForwarding)
            throws IOException {
        if (localForwarding)
            log.info(session + " establishing local port forwarding from " + local + " to " + remote);
        else
            log.info(session + " establishing remote port forwarding from " + remote + " to " + local);
    }

    /**
     * Signals a successful/failed attempt to establish a local/remote port forwarding
     *
     * @param  session         The {@link Session} through which the attempt was made
     * @param  local           The local address - may be {@code null} on the receiver side
     * @param  remote          The remote address - may be {@code null} on the receiver side
     * @param  localForwarding Local/remote port forwarding indicator
     * @param  boundAddress    The bound address - non-{@code null} if successful
     * @param  reason          Reason for failure - {@code null} if successful
     * @throws IOException     If failed to handle the event - in which case the established tunnel is aborted
     */
    public void establishedExplicitTunnel(
            Session session, SshdSocketAddress local, SshdSocketAddress remote, boolean localForwarding,
            SshdSocketAddress boundAddress, Throwable reason)
            throws IOException {
        if (localForwarding)
            log.info(session + " established local port forwarding from " + local + " to " + remote + " bound to " + boundAddress + ": " + reason);
        else
            log.info(session + " established remote port forwarding from " + remote + " to " + local + " bound to " + boundAddress + ": " + reason);
    }

    /**
     * Signals a request to tear down a local/remote port forwarding
     *
     * @param  session         The {@link Session} through which the request is made
     * @param  address         The (bound) address - local/remote according to the forwarding type
     * @param  localForwarding Local/remote port forwarding indicator
     * @param  remoteAddress   The specified peer address when tunnel was established - may be {@code null} for
     *                         server-side local tunneling requests
     * @throws IOException     If failed to handle the event - in which case the request is aborted
     */
    public void tearingDownExplicitTunnel(
            Session session, SshdSocketAddress address, boolean localForwarding, SshdSocketAddress remoteAddress)
            throws IOException {
        if (localForwarding)
            log.info(session + " tearing down local port forwarding from " + address + " to " + remoteAddress);
        else
            log.info(session + " tearing down remote port forwarding from " + remoteAddress + " to " + address);
    }

    /**
     * Signals a successful/failed request to tear down a local/remote port forwarding
     *
     * @param  session         The {@link Session} through which the request is made
     * @param  address         The (bound) address - local/remote according to the forwarding type
     * @param  localForwarding Local/remote port forwarding indicator
     * @param  remoteAddress   The specified peer address when tunnel was established - may be {@code null} for
     *                         server-side local tunneling requests
     * @param  reason          Reason for failure - {@code null} if successful
     * @throws IOException     If failed to handle the event - <B>Note:</B> the exception is propagated, but the port
     *                         forwarding may have been torn down - no rollback
     */
    public void tornDownExplicitTunnel(
            Session session, SshdSocketAddress address, boolean localForwarding, SshdSocketAddress remoteAddress,
            Throwable reason)
            throws IOException {
        if (localForwarding)
            log.info(session + " torn down local port forwarding from " + address + " to " + remoteAddress + ": " + reason);
        else
            log.info(session + " torn down remote port forwarding from " + remoteAddress + " to " + address + ": " + reason);
    }
}
