package org.sftp.util;

import org.apache.sshd.common.channel.Channel;
import org.apache.sshd.common.channel.ChannelListener;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.slf4j.LoggerFactory;

/**
 * A {@link ChannelListener} that logs the channel events.
 */
public class ChannelEventLogger extends AbstractLoggingBean implements ChannelListener {
    String name; // Name of the class using this logger
    public ChannelEventLogger(String name) {
        super(LoggerFactory.getLogger(ChannelEventLogger.class.getSimpleName()));
        this.name = name;
    }

    /**
     * Called to inform about initial setup of a channel via the
     * {@link Channel#init(org.apache.sshd.common.session.ConnectionService, org.apache.sshd.common.session.Session, long)}
     * method. <B>Note:</B> this method is guaranteed to be called before either of the
     * {@link #channelOpenSuccess(Channel)} or {@link #channelOpenFailure(Channel, Throwable)} will be called
     *
     * @param channel The initialized {@link Channel}
     */
    public void channelInitialized(Channel channel) {
        log.info(name + " channel initialized: " + channel);
    }

    /**
     * Called to inform about a channel being successfully opened for a session. <B>Note:</B> when the call is made, the
     * channel is known to be open but nothing beyond that.
     *
     * @param channel The newly opened {@link Channel}
     */
    public void channelOpenSuccess(Channel channel) {
        log.info(name + " channel open success: " + channel);
    }

    /**
     * Called to inform about a channel being closed. <B>Note:</B> when the call is made there are no guarantees about
     * the channel's actual state except that it either has been already closed or may be in the process of being
     * closed. <B>Note:</B> this method is guaranteed to be called regardless of whether
     * {@link #channelOpenSuccess(Channel)} or {@link #channelOpenFailure(Channel, Throwable)} have been called
     *
     * @param channel The referenced {@link Channel}
     * @param reason  The reason why the channel is being closed - if {@code null} then normal closure
     */
    public void channelClosed(Channel channel, Throwable reason) {
        log.info(name + " channel closed: " + channel + ", reason: " + reason);
    }
}
