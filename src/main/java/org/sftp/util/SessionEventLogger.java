package org.sftp.util;

import org.apache.sshd.common.session.Session;
import org.apache.sshd.common.session.SessionListener;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.slf4j.LoggerFactory;

/**
 * A {@link SessionListener} that logs the session events.
 */
public class SessionEventLogger extends AbstractLoggingBean implements SessionListener {
    String name; // Name of the class using this logger
    public SessionEventLogger(String name) {
        super(LoggerFactory.getLogger(SessionEventLogger.class.getSimpleName()));
        this.name = name;
    }

    /**
     * A new session just been created
     *
     * @param session The created {@link Session}
     */
    public void sessionCreated(Session session) {
        log.info(name + " session created: " + session);
    }

    /**
     * A session has been closed
     *
     * @param session The closed {@link Session}
     */
    public void sessionClosed(Session session) {
        log.info(name + " session closed: " + session);
    }
}
