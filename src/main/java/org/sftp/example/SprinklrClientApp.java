package org.sftp.example;

import org.sftp.core.SftpClient;

import java.security.KeyPair;

public class SprinklrClientApp {
    public static void main(String[] args) {
        // Create the SFTP client
        SftpClient client = new SftpClient();

        try {
            String configFile = "./src/test/resources/sprinklr-sftp-config.json";
            client.connect(configFile);


            // Perform SFTP operations

            // List files in the current remote directory with flags -la
            client.ls(".", "-la");

            // List files in ./dev dir with flags -a
            client.ls("./dev", "-a");

            // Get file ReadMe.md and save it in ./Downloads/ReadMe.md
//            client.put("ReadMe.md", "./downloads/ReadMe.md");

            // Disconnect from the server
            client.disconnect();
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
