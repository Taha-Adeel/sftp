package org.sftp.test;

import org.junit.jupiter.api.Test;
import org.sftp.core.SftpServer;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Unit tests for {@link SftpServer}.
 */
class SftpServerTest {
    int testPort = 2225;
    String testUsername = "server-user";
    String testPassword = "server-password";
    String testKeyPath = "./src/test/resources/server_key.pem";

    /**
     * Test that the SFTP server starts and stops successfully.
     * @throws IOException
     */
    @Test
    public void testStartAndStopSftpServer() throws IOException {
        System.out.println("testStartAndStopSftpServer() - start");
        SftpServer sftpServer = new SftpServer(testUsername, testPassword, testPort, testKeyPath);
        sftpServer.start();
        assertTrue(sftpServer.getSshServer().isStarted());
        sftpServer.stop();
        assertFalse(sftpServer.getSshServer().isStarted());
        System.out.println("testStartAndStopSftpServer() - passed()");
    }

    /**
     * Test the port configuration of the SFTP server.
     */
    @Test
    public void testPortConfiguration() {
        System.out.println("testPortConfiguration() - start");
        SftpServer sftpServer = new SftpServer(testUsername, testPassword, testPort, testKeyPath);
        assertEquals(testPort, sftpServer.getSshServer().getPort());
        System.out.println("testPortConfiguration() - passed()");
    }

    /**
     * Test that the SFTP server authenticates the user correctly.
     */
    @Test
    public void testAuthentication() {
        System.out.println("testAuthentication() - start");
        SftpServer sftpServer = new SftpServer(testUsername, testPassword, testPort, testKeyPath);
        assertTrue(sftpServer.getSshServer().getPasswordAuthenticator().authenticate("server-user", "server-password", null));
        assertFalse(sftpServer.getSshServer().getPasswordAuthenticator().authenticate("server-user", "wrong_password", null));
        System.out.println("testAuthentication() - passed()");
    }

}