package org.sftp.example;

import org.sftp.core.SftpClient;
import org.sftp.core.SftpServer;

import java.security.KeyPair;

// Runs an SFTP client that connects to server-user@localhost:2225 and performs SFTP operations. Assumes that the server is running.
public class SftpClientApp {
    public static void main(String[] args) {
        // Create the SFTP client
        SftpClient client = new SftpClient();

        try {
            // Password authentication
            client.connect("localhost", 2225, "server-user", "server-password");

            /*
                Perform SFTP operations
            */

            // List files in the current remote directory
            client.ls();

            // List files in ./src dir with flags -l
            client.ls(".", "-la");

            // List files in current dir with flags -la
            client.ls("./dir", "-a");

            // Get file ReadMe.md and save it in ./Downloads/ReadMe.md
            client.get("./A.txt", "./Downloads/A_copy.txt");

            // Put the file ./Downloads/A_copy.txt into ./Uploads/A_copy.txt on the remote server
            client.put("./Downloads/A_copy.txt", "./Uploads/A_copy.txt");

            // Delete the file ./Uploads/A_copy.txt
            client.rm("./Uploads/A_copy.txt");

            // Get the remote root directory and save it in ./server_files
            client.getDir(".", "./server_files");

            // Put the directory ./server_files/dir into ./Uploads/dir on the remote server
            client.putDir("./server_files/dir", "./Uploads/dir");

            // Delete the directory ./Uploads/dir
            client.rmdir("./Uploads");

            // Disconnect from the server
            client.disconnect();
        } catch (Exception e) {
            System.out.println("Exception: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
