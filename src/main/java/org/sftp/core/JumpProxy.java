package org.sftp.core;

import org.apache.sshd.common.io.nio2.Nio2ServiceFactoryFactory;
import org.apache.sshd.common.util.logging.AbstractLoggingBean;
import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.auth.pubkey.AcceptAllPublickeyAuthenticator;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.config.keys.AuthorizedKeysAuthenticator;
import org.apache.sshd.server.forward.AcceptAllForwardingFilter;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.sftp.util.ChannelEventLogger;
import org.sftp.util.PortForwardingEventLogger;
import org.sftp.util.SessionEventLogger;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * {@link JumpProxy} sets up an SSH jump proxy server that allows users to connect to a remote server through it.
 * It contains an SSH server instance that listens on a specified port and authenticates users using a username and
 * password or public key and serves as jump proxy by making a TCP forwarding connection to the remote server.
 */
public class JumpProxy extends AbstractLoggingBean {
    private final SshServer sshServer; // The SSH server instance
    private String authorizedKeysFilePath; // The file path to the authorized keys

    /**
     * Constructs a new instance of {@link JumpProxy} with the specified parameters.
     *
     * @param username                  the username for authentication
     * @param password                  the password for authentication
     * @param port                      the port on which the proxy server will listen.
     * @param keyFilePath               the file path to the SSH host key. Creates a new key if file does not exist.
     * @param authorizedKeysFilePath    the file path to the authorized keys. Accepts all keys if null.
     */
    public JumpProxy(String username, String password, int port, String keyFilePath, String authorizedKeysFilePath) {
        sshServer = SshServer.setUpDefaultServer();

        sshServer.setPort(port);
        sshServer.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(Paths.get(new File(keyFilePath).getAbsolutePath())));
        sshServer.setPasswordAuthenticator((user, pass, session) -> user.equals(username) && pass.equals(password));
        sshServer.setIoServiceFactoryFactory(new Nio2ServiceFactoryFactory());

        // Set the public key authenticator to accept all keys if authorizedKeysFilePath is null or the path to the authorized keys file to allow password-less authentication
        PublickeyAuthenticator publicKeyAuthenticator;
        this.authorizedKeysFilePath = authorizedKeysFilePath;
        if(authorizedKeysFilePath == null)
            publicKeyAuthenticator = AcceptAllPublickeyAuthenticator.INSTANCE;
        else
            publicKeyAuthenticator = new AuthorizedKeysAuthenticator(Paths.get(new File(authorizedKeysFilePath).getAbsolutePath()));
        sshServer.setPublickeyAuthenticator(publicKeyAuthenticator);

        // Enable TCP forwarding
        sshServer.setForwardingFilter(AcceptAllForwardingFilter.INSTANCE);

        // Add event listeners
        sshServer.addPortForwardingEventListener(new PortForwardingEventLogger("Jump Proxy"));
        sshServer.addChannelListener(new ChannelEventLogger("Proxy"));
        sshServer.addSessionListener(new SessionEventLogger("Proxy"));

        log.info("Jump proxy set up on port " + port + " with username \"" + username + "\" and password \"" + password + "\"");
    }

    /**
     * Return the underlying {@link SshServer} instance.
     * @return the {@link SshServer} instance
     */
    public SshServer getSshServer() {
        return sshServer;
    }

    /**
     * Starts the jump proxy server
     *
     * @throws IOException if an I/O error occurs
     */
    public void start() throws IOException {
        sshServer.start();
        log.info("Jump proxy started");
    }

    /**
     * Stops the jump proxy server
     *
     * @throws IOException if an I/O error occurs
     */
    public void stop() throws IOException {
        sshServer.stop();
        log.info("Jump proxy stopped");
    }

    /**
     * Sets the authorized keys file path.
     * @param authorizedKeysFilePath the file path to the authorized keys
     */
    public void setAuthorizedKeysFilePath(String authorizedKeysFilePath) {
        this.authorizedKeysFilePath = authorizedKeysFilePath;
        sshServer.setPublickeyAuthenticator(new AuthorizedKeysAuthenticator(Paths.get(new File(authorizedKeysFilePath).getAbsolutePath())));
    }
}
