package org.sftp.test;

import org.apache.sshd.server.SshServer;
import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.sftp.server.SftpSubsystemFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.sftp.core.SftpClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Unit tests for {@link SftpClient}
 */
class SftpClientTest {
    SshServer testServer;
    String testHost = "localhost";
    int testPort = 2225;
    String testUsername = "server-user";
    String testPassword = "server-password";
    String testKeyPath = "./src/test/resources/server_key.pem";

    SftpClient sftpClient;

    /**
     * Set up the ssh server before each test
     */
    @BeforeEach
    void setUp() throws IOException {
        testServer = SshServer.setUpDefaultServer();
        testServer.setPort(testPort);
        testServer.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(Paths.get(new File(testKeyPath).getAbsolutePath())));
        testServer.setPasswordAuthenticator((username, password, session) -> username.equals(testUsername) && password.equals(testPassword));
        testServer.setSubsystemFactories(Collections.singletonList(new SftpSubsystemFactory.Builder().build()));

        testServer.start();
        sftpClient = new SftpClient();
    }

    /**
     * Tear down the ssh server after each test
     */
    @AfterEach
    void tearDown() throws IOException {
        sftpClient.disconnect();
        testServer.stop();
    }

    /**
     * Test connecting to the server with a username and password
     * @throws IOException
     */
    @Test
    void testConnectPassword() throws IOException {
        System.out.println("testConnectPassword() started");

        // Arrange
        assertNull(sftpClient.getSession());
        assertNull(sftpClient.getSftpClient());

        // Act
        sftpClient.connect(testHost, testPort, testUsername, testPassword);

        // Assert
        assertNotNull(sftpClient.getSession());
        assertNotNull(sftpClient.getSftpClient());

        // Check that the session is connected and open
        assertTrue(sftpClient.getSession().isAuthenticated());
        assertTrue(sftpClient.getSession().isOpen());

        // Check that the sftp client is connected and open
        assertTrue(sftpClient.getSftpClient().isOpen());

        System.out.println("testConnectPassword() passed\n");
    }

    @Test
    void testConnectKey() {
    }


    /**
     * Test the ls() method with no arguments on the actual sftp server
     */
    @Test
    void testLsServer() throws IOException {
        System.out.println("testLsServer() started");

        // Arrange
        sftpClient.connect(testHost, testPort, testUsername, testPassword);

        // Set up the output stream for capturing the console output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream originalOut = System.out;
        System.setOut(printStream);

        // Act
        sftpClient.ls();

        // Assert
        String consoleOutput = outputStream.toString();
        assertFalse(consoleOutput.contains(".idea"));
        assertTrue(consoleOutput.contains("build.gradle"));

        // Clean up
        System.setOut(originalOut);
        System.out.println("ls output:");
        System.out.print(consoleOutput);
        System.out.println("testLsServer() passed\n");
        printStream.close();
        outputStream.close();
    }

    /**
     * Test the ls() method with the "-la" option on the actual sftp server
     */
    @Test
    void testLsLaServer() throws IOException {
        System.out.println("testLsLaServer() started");

        // Arrange
        sftpClient.connect(testHost, testPort, testUsername, testPassword);

        // Set up the output stream for capturing the console output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream originalOut = System.out;
        System.setOut(printStream);

        // Act
        sftpClient.ls(".", "la");

        // Assert
        String consoleOutput = outputStream.toString();
        assertTrue(consoleOutput.contains(".idea"));
        assertTrue(consoleOutput.contains("build.gradle"));

        // Clean up
        System.setOut(originalOut);
        System.out.println("ls -la output:");
        System.out.print(consoleOutput);
        System.out.println("testLsLaServer() passed\n");
        printStream.close();
        outputStream.close();
    }

    /**
     * Test the ls() method with the "-la" option and mock the behavior of SftpClient
     */
    @Test
    void testLsLaMock() throws IOException {
        System.out.println("testLsLaMock() started");

        // Arrange
        String remotePath = "/path/to/remote/directory";
        String options = "la";

        // Create some test directory entries
        org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry1 = createDirEntry(".hidden", 100, 1630764000L);
        org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry2 = createDirEntry("file.txt", 200, 1630864000L);
        Collection<org.apache.sshd.sftp.client.SftpClient.DirEntry> dirEntries = Arrays.asList(dirEntry1, dirEntry2);

        // Mock the behavior of SftpClient
        sftpClient.sftpClient = mock(org.apache.sshd.sftp.client.SftpClient.class);
        when(sftpClient.getSftpClient().readEntries(remotePath)).thenReturn(dirEntries);

        // Set up the output stream for capturing the console output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(outputStream);
        PrintStream originalOut = System.out;
        System.setOut(printStream);

        // Act
        sftpClient.ls(remotePath, options);

        // Assert
        String consoleOutput = outputStream.toString();
        assertTrue(consoleOutput.contains(".hidden"));
        assertTrue(consoleOutput.contains("file.txt"));
        assertTrue(consoleOutput.contains("100"));
        assertTrue(consoleOutput.contains("200"));

        // Clean up
        System.setOut(originalOut);
        System.out.println("ls " + remotePath + " -la output:");
        System.out.print(consoleOutput);
        System.out.println("testLsLaMock() passed\n");
        printStream.close();
        outputStream.close();
    }

    /**
     * Create a mock SftpClient.DirEntry object
     * @param filename The name of the file
     * @param size The size of the file
     * @param modifyTime The last modified time of the file
     * @return The mock SftpClient.DirEntry object
     */
    private org.apache.sshd.sftp.client.SftpClient.DirEntry createDirEntry(String filename, long size, long modifyTime) {
        org.apache.sshd.sftp.client.SftpClient.Attributes attributes = mock(org.apache.sshd.sftp.client.SftpClient.Attributes.class);
        when(attributes.getSize()).thenReturn(size);
        when(attributes.getModifyTime()).thenReturn(FileTime.fromMillis(modifyTime));

        org.apache.sshd.sftp.client.SftpClient.DirEntry dirEntry = mock(org.apache.sshd.sftp.client.SftpClient.DirEntry.class);
        when(dirEntry.getFilename()).thenReturn(filename);
        when(dirEntry.getAttributes()).thenReturn(attributes);

        return dirEntry;
    }

    /**
     * Test the get() method with actual SftpServer
     */
    @Test
    void testGetServer() throws IOException {
        System.out.println("testGetServer() started");

        // Arrange
        sftpClient.connect(testHost, testPort, testUsername, testPassword);
        String remotePath = testKeyPath;
        String localName = "./my_key_downloaded_copy.pub";

        // Act
        sftpClient.get(remotePath, localName);

        // Assert
        File downloadedFile = new File(localName);
        assertTrue(downloadedFile.exists());
        assertTrue(downloadedFile.isFile());
        assertNotEquals(downloadedFile.length(), 0);

        // Clean up. Delete the downloaded file
        var del = downloadedFile.delete();

        System.out.println("testGetServer() passed\n");
    }

    /**
     * Test the get() method with mock SftpClient
     */
    @Test
    void testGetMock() throws IOException {
        System.out.println("testGetMock() started");

        // Arrange
        sftpClient.sftpClient = mock(org.apache.sshd.sftp.client.SftpClient.class);
        String remotePath = "/path/to/remote/file.txt";
        String localName = "downloaded_file.txt";

        // Mock the behavior of SftpClient
        FileChannel remoteFileChannel = FileChannel.open(Paths.get(testKeyPath));
        when(sftpClient.getSftpClient().openRemoteFileChannel(remotePath)).thenReturn(remoteFileChannel);

        // Act
        sftpClient.get(remotePath, localName);

        // Assert
        verify(sftpClient.getSftpClient()).openRemoteFileChannel(remotePath);

        // Additional assertions
        assertEquals(localName, "downloaded_file.txt");

        // Clean up. Delete the downloaded file
        File downloadedFile = new File(localName);
        //noinspection ResultOfMethodCallIgnored
        downloadedFile.delete();

        System.out.println("testGetMock() passed\n");
    }

    /**
     * Test the put() method with actual SftpServer
     */
    @Test
    void testPutServer() throws IOException {
        System.out.println("testPutServer() started");

        // Arrange
        sftpClient.connect(testHost, testPort, testUsername, testPassword);
        String localPath = testKeyPath;
        String remoteName = "./my_key_uploaded_copy.pub";

        // Act
        sftpClient.put(localPath, remoteName);

        // Assert
        File uploadedFile = new File(remoteName);
        assertTrue(uploadedFile.exists());
        assertTrue(uploadedFile.isFile());
        assertNotEquals(uploadedFile.length(), 0);

        // Clean up. Delete the downloaded file
        var del = uploadedFile.delete();

        System.out.println("testPutServer() passed\n");
    }

//    @Test
//    void testPutMock() throws IOException {
//        System.out.println("testPutMock() started");
//
//        // Arrange
//        mySftpClient.sftpClient = mock(SftpClient.class);
//        String localPath = "server_key.pub";
//        String remoteName = "remote_file.txt";
//
//        // Mock the behavior of SftpClient
//        FileChannel remoteFileChannel = mock(FileChannel.class);
//        when(mySftpClient.getSftpClient().openRemoteFileChannel(remoteName,  EnumSet.of(SftpClient.OpenMode.Write, SftpClient.OpenMode.Create))).thenReturn(remoteFileChannel);
//
//        // Act
//        mySftpClient.put(localPath, remoteName);
//
//        // Assert
//        verify(mySftpClient.getSftpClient()).openRemoteFileChannel(remoteName);
//        verify(remoteFileChannel).size();
//
//        // Additional assertions
//        assertEquals(remoteName, "/path/to/remote/file.txt");
//
//        // Clean up. Delete the downloaded file
//        File uploadedFile = new File(remoteName);
//        //noinspection ResultOfMethodCallIgnored
//        uploadedFile.delete();
//
//        System.out.println("testPutMock() passed\n");
//    }
}